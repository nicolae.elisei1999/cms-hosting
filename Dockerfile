# Install dependencies only when needed
FROM node:16-alpine AS build
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat

WORKDIR /app

#RUN npm install --frozen-lockfile

COPY package.json  .
COPY package-lock.json .
COPY .env.* .
COPY next.config.js .

RUN npm install

COPY . .

RUN npm run build

# Rebuild the source code only when needed

FROM node:16-alpine

WORKDIR /app

# Production image, copy all the files and run next
ENV NODE_ENV production

# Again get dependencies, but this time only install
# runtime dependencies
COPY package.json  .
COPY package-lock.json .
COPY .env.* .
COPY next.config.js .
RUN npm install --only=production

# Get the built application from the first stage
COPY --from=build /app/.next/ /app/.next/
COPY --from=build /app/public ./public

# Set runtime metadata
ENV PORT 3000
EXPOSE 3000

RUN ls /app -R

# CMD ["npm", "start"]
